#pragma once
#include "overlay/hijack/hijack.h"
#include "driver/process.h"
#include "driver/driver.h"
#include "game/sdk.h"
#define xor_a(x) x

#define ERROR(msg)                           \
	std::cout << "[-] " << msg << std::endl; \
	std::cin.get();                          \
	exit(EXIT_FAILURE);
#define ASSERT(cond, msg) \
	if (!cond)            \
	{                     \
		ERROR(msg)        \
	}
vec2_t resolution(1920, 1080);
bool aimbot = true;
int fov = 50;
bool aimHead = true;
int actorAim = 0;
bool withName = true;

std::string getAimbotString()
{
	if (!aimHead)
	{
		return "body";
	}
	return "head";
}

void showLog(bool withAddress)
{
	system("CLS");
	std::cout << "By TekMonts#6635, working with version 1.50.2.xxxxxxxx" << std::endl;
	std::cout << "Set game to window borderless, resolution is 1920x1080" << std::endl;
	std::cout << "[-] Hot Key" << std::endl;
	std::cout << "	[~] F7/F8/F9: Aimbot FOV 25/50/100 (Current: " << fov << ")" << std::endl;
	std::cout << "	[~] F10: Aimbot enable: " << std::boolalpha << aimbot << std::endl;
	std::cout << "	[~] F11: Aimbot body/head - Current: " << getAimbotString() << std::endl;
	if (withAddress)
	{
		std::cout << "[-] Log" << std::endl;
		std::cout << "	[~] usermode pid:  " << usermode_pid << std::endl;
		std::cout << "	[~] m_pid: " << m_pid << std::endl;
		std::cout << "	[~] m_base:  " << std::hex << m_base << std::endl;
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

std::string getInforString()
{
	std::string inforString;
	inforString = "TekMonts#6635";
	//inforString += " | Aimbot: ";
	//inforString += aimbot ? getAimbotString() + " - fov: " + std::to_string(static_cast<int>(fov)) : "false";
	return inforString;
}

std::string gen_random(const int len)
{
	static const char alphanum[] =
		"3274654325"
		"nfbashgfmafghaeqqwertyuiop"
		"MNVCGFJKASGFJHASGKALHRRQLQ";
	std::string tmp_s;
	tmp_s.reserve(len);

	for (int i = 0; i < len; ++i)
	{
		tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];
	}
	return tmp_s;
}

bool verify_game()
{
	m_driver_control = kernel_control_function();

	if (!m_driver_control)
		return false;

	m_pid = PIDManager::GetAowProcId();

	if (!m_pid)
		return false;
	sdk::process_id = m_pid;
	std::cout << "sdk::process_id: " << sdk::process_id << std::endl;
	sdk::set_game_hwnd();
	std::cout << "sdk::process_id: " << sdk::process_id << std::endl;
	ASSERT(sdk::hwnd, "failed to find window handle");
	ASSERT(hijack::init(), "failed to hijack nvidia overlay");
	ASSERT(renderer::init(), "failed to initlize renderer");
	usermode_pid = GetCurrentProcessId();

	m_base = GetBaseAddress();

	if (!m_base)
		return false;

	//ClearPiddb();
	//FindMmDriverData();
	//ClearMmUnloadedDrivers();
	//Sleep(200);
	return true;
}
int main()
{

	SetConsoleTitle(gen_random(17).c_str());
	if (verify_game()) {
		showLog(true);
	}
	else {
		showLog(false);
		std::cout << "By TekMonts#6635, working with version 1.50.2.xxxxxxxx" << std::endl;;
	}
	while (!GetAsyncKeyState(VK_DELETE))
	{
		system("CLS");
		renderer::scene::start();
		vec2_t middle(resolution.x / 2 - 8, resolution.y / 2 - 14);
		std::string inforStr = getInforString();
		CA2W pszWide(inforStr.c_str(), CP_UTF8);
		renderer::scene::text(middle, L"+", renderer::colors::red_color, renderer::fonts::watermark_font);
		renderer::scene::text(vec2_t(resolution.x - inforStr.size() * 6 - 10, 10), pszWide, renderer::colors::test_color, renderer::fonts::tahoma_font);


		vec2_t Screen;

		DWORD_PTR PtrRender = RPMDPTR(m_base + offsets::adrRender);
		std::cout << "	[~] PtrRender:  " << std::hex << PtrRender << std::endl;
		sdk::Matrix44 DrawMatrix = read<sdk::Matrix44>(PtrRender + offsets::Render_Matrix);
		std::cout << "	[~] DrawMatrix:  " << DrawMatrix._11 << PtrRender << std::endl;
		DWORD_PTR p1 = RPMDPTR(m_base + offsets::AdrApplication);
		std::cout << "	[~] p1:  " << std::hex << p1 << std::endl;
		if (p1)
		{
			/*for (int i = 0x0; i < 1024; i++) {
				std::cout << "	[~] 0x" << std::hex << i << ": " << std::hex << RPMDPTR(p1+(i*8)) << std::endl;
			}*/
			DWORD_PTR p2 = RPMDPTR(p1 + offsets::ptrArray1);
			std::cout << "	[~] p2:  " << std::hex << p2 << std::endl;
			if (p2)
			{
				sdk::Vector Objects = read<sdk::Vector>(p2 + offsets::ArrayMap);
				std::cout << "	[~] Objects.End:  " << std::hex << Objects.End << std::endl;
				sdk::AzList CurList = read<sdk::AzList>(Objects.Begin);
				std::cout << "	[~] CurList.Begin:  " << std::hex << CurList.Begin << std::endl;

				while (CurList.Begin != Objects.End)
				{
					if (is_valid_ptr(CurList.Data))
					{
						DWORD_PTR Transform = RPMDPTR(CurList.Data + offsets::Object_Transform);
						std::cout << "	[~] Transform:  " << std::hex << Transform << std::endl;
						if (is_valid_ptr(Transform))
						{
							sdk::Matrix34 WorldTM = read<sdk::Matrix34>(Transform + offsets::Transform_Matrix);

							vec3_t Pos = WorldTM.GetTranslation();
							std::cout << "	[~] pos: x=" << Pos.x << ", y=" << Pos.y << ", z=" << Pos.z << std::endl;
							if (WorldToScreen(Pos, Screen, DrawMatrix))
							{
								//std::string Name;
								////char Name[128];
								////ZeroMemory(&Name, sizeof(Name));
								//UINT CountName = read<UINT>(CurList.Data + offsets::Object_CountName);
								//if (CountName > 16)
								//{


								//	DWORD_PTR PtrName = RPMDPTR(CurList.Data + offsets::Object_Name);
								//	if (PtrName)
								//	{
								//		Name = read<std::string>(PtrName);
								//	}
								//}
								//else
								//{
								//	Name = read<std::string>(CurList.Data + offsets::Object_Name);
								//}

								bool isInside = isInSideFOV(Screen, middle);

								DrawESP(Screen, "Object", 0, isInside);
							}
						}
					}

					if (is_valid_ptr(CurList.Begin))
					{
						CurList = read<sdk::AzList>(CurList.Begin);
					}
					else
					{
						std::cout << "break:  " << std::endl;
						continue;
					}
				}
			}
		}

		renderer::scene::end();
	}
	ClearPiddb();
	FindMmDriverData();
	ClearMmUnloadedDrivers();
	renderer::shutdown();
	return 0;
}
