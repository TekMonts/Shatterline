#pragma once
#include "internal.h"
#include "../../utils/vectors.h"

namespace renderer {
	namespace colors {
		extern ID2D1SolidColorBrush* white_color;
		extern ID2D1SolidColorBrush* red_color;
		extern ID2D1SolidColorBrush* green_color;
		extern ID2D1SolidColorBrush* blue_color;
		extern ID2D1SolidColorBrush* black_color;
		extern ID2D1SolidColorBrush* test_color;
		void getColor(D2D1::ColorF color, ID2D1SolidColorBrush*& outColor);
		bool init();
		void shutdown();
	}

	namespace fonts {
		extern IDWriteTextFormat* watermark_font;
		extern IDWriteTextFormat* tahoma_font;
		bool init();
		void shutdown();
	}

	namespace scene {
		void start(); 
		void begin();
		void end();
		void clear();
		void shutdown();

		void line(float x, float y, float x1, float y1, float width, ID2D1SolidColorBrush* color/*ARGB*/);
		void text(vec2_t pos, const wchar_t* text, ID2D1SolidColorBrush* color, IDWriteTextFormat* font);
		void box(float x, float y, float width, float height, float thick, ID2D1SolidColorBrush* color);
		void ellipse(float x, float y, float size, ID2D1SolidColorBrush* color);
		void fillRectangle(float left, float right, float top, float bottom, ID2D1SolidColorBrush* color);
	}

	bool init();
	void shutdown();
}