#include <iostream>
#include "driver/driver.h"
#include "game/offsets.h"
#include "overlay/hijack/hijack.h"
#include "overlay/renderer/renderer.h"
#include <thread>
#include <string>
#include <cstdlib>
#include <ctime>

#define ERROR(msg) std::cout << "[-] " << msg << std::endl; std::cin.get(); exit(EXIT_FAILURE); 
#define ASSERT(cond, msg) if (!cond) { ERROR(msg) }
vec2_t resolution(1920, 1080);
bool aimbot = true;
bool flag = false;
int fov = 50;
int aim_location = 5;
bool ingameFlag = false;
int player_count = 150;
bool recoil = false;
int actorAim = 0;
bool uav = false;

std::string getAimbotString()
{
	if (aim_location == 5) {
		return "body";
	}
	return "head";
}

void showLog(bool withAddress)
{
	system("CLS");
	std::cout << "By TekMonts#6635, working with version 1.45.2.10635118" << std::endl;
	std::cout << "Set game to window borderless, resolution is 1920x1080" << std::endl;
	std::cout << "[-] Hot Key" << std::endl;
	std::cout << "	[~] F4: Advance UAV: " << std::boolalpha << uav << " (testing, will update asap)" <<std::endl;
	std::cout << "	[~] F5: No recoil enable: " << std::boolalpha << recoil << std::endl;
	std::cout << "	[~] F6: Reload address (Each game)" << std::endl;
	std::cout << "	[~] F7/F8/F9: Aimbot FOV 25/50/100 (Current: " << fov << ")" << std::endl;
	std::cout << "	[~] F10: Aimbot enable: " << std::boolalpha << aimbot << std::endl;
	std::cout << "	[~] F11/F12: Aimbot body/head (Current: " << getAimbotString() << ")" << std::endl;
	if (withAddress)
	{
		std::cout << "[-] Log" << std::endl;
		std::cout << "	[~] pid:  " << std::hex << sdk::process_id << std::endl;
		std::cout << "	[~] base: " << std::hex << sdk::module_base << std::endl;
		std::cout << "	[~] peb:  " << std::hex << sdk::peb << std::endl;
		std::cout << "	[~] hwnd: " << std::hex << sdk::hwnd << std::endl;
		std::cout << "	[~] client_info:      " << std::hex << sdk::client_info << std::endl;
		std::cout << "	[~] client_info_base: " << std::hex << sdk::client_info_base << std::endl;
		std::cout << "	[~] visible_base: " << std::hex << sdk::visible_base << std::endl;
		std::cout << "	[~] in game: " << std::boolalpha << sdk::in_game() << std::endl;
		std::cout << "	[~] player count: " << std::dec << sdk::player_count() << std::endl;
		std::cout << std::endl;
		if (!sdk::in_game())
		{
			std::cout << "Currently not in game... waiting for new game... " << std::endl;
		}
	}
	std::cout << std::endl;

}


std::string gen_random(const int len) {
	static const char alphanum[] =
		"9876543210"
		"abcdefghijklmnopqrstuvwxyz"
		"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	std::string tmp_s;
	tmp_s.reserve(len);

	for (int i = 0; i < len; ++i) {
		tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];
	}

	return tmp_s;
}

#define ADDRESS_CG_T 0x180E4290 // [[48 63 C1 48 69 C8 60 03 00 00 48 8D ? ? ? ? ? 48 03 C8 E9] - 0xA] lea - 0x18
void aUAV() 
{
	uintptr_t cg = driver::read<uintptr_t>(ADDRESS_CG_T);
	if (cg) {
		int health = driver::read<int>(cg + 0x25C);
		if (health >= 0 && health <= 300)
		{
			driver::write<int>(cg + 0x304, 33619969, sizeof(int));
		}
	}
}

void initAdd()
{
	flag = false;
	sdk::client_info = decryption::get_client_info();
	if (!sdk::client_info or sdk::client_info == 0) {
		flag = false;
		showLog(false);
		std::cout << "Cannot found client_info... waiting for new game..." << std::endl;
		return;
	}
	const auto bone_base_pos = decryption::get_bone_base_pos(sdk::client_info);

	if (bone_base_pos.x == 0 and bone_base_pos.y == 0 and bone_base_pos.z == 0) {
		flag = false;
		showLog(false);
		std::cout << "Cannot found bone_base_pos... waiting for new game..." << std::endl;
		return;
	}
	sdk::client_info_base = decryption::get_client_info_base();
	auto ref_def_ptr = decryption::get_ref_def();
	if (!ref_def_ptr or ref_def_ptr == 0) {
		flag = false;
		showLog(false);
		std::cout << "Cannot found ref_def... waiting for new game..." << std::endl;
		return;
	}
	sdk::visible_base = sdk::get_visible_base(sdk::module_base);
	sdk::ref_def = driver::read<sdk::ref_def_t>(ref_def_ptr);
	ingameFlag = sdk::in_game();
	showLog(true);
	flag = true;
};

//void checkCondition()
//{
//	while (true) {
//		if (GetAsyncKeyState(VK_F5)) { recoil = !recoil; Sleep(100); showLog(true); }
//		if (GetAsyncKeyState(VK_F6)) { initAdd(); Sleep(100); showLog(true); }
//		if (GetAsyncKeyState(VK_F7)) { fov = 25; showLog(true); }
//		if (GetAsyncKeyState(VK_F8)) { fov = 50; showLog(true); }
//		if (GetAsyncKeyState(VK_F9)) { fov = 100; showLog(true); }
//		if (GetAsyncKeyState(VK_F10)) { aimbot = !aimbot; Sleep(100); showLog(true); }
//		if (GetAsyncKeyState(VK_F11)) { aim_location = 5; showLog(true); }
//		if (GetAsyncKeyState(VK_F12)) { aim_location = 7; showLog(true); }
//		Sleep(100);
//		/*Sleep(30000);
//		if (sdk::in_game()) {
//			ingameFlag = true;
//			player_count = sdk::player_count();
//			continue;
//		}
//		ingameFlag = false;
//		flag = false;*/
//	}
//}

int main() {
	sdk::process_id = driver::get_process_id("ModernWarfare.exe");
	ASSERT(sdk::process_id, "failed to find pid");

	sdk::module_base = driver::get_module_base_address("ModernWarfare.exe");
	ASSERT(sdk::module_base, "failed to module base");

	sdk::peb = driver::get_peb();
	ASSERT(sdk::peb, "failed to find peb");

	sdk::set_game_hwnd();
	ASSERT(sdk::hwnd, "failed to find window handle");

	ASSERT(hijack::init(), "failed to hijack nvidia overlay");
	ASSERT(renderer::init(), "failed to initlize renderer");

	SetConsoleTitle(gen_random(16).c_str());

	showLog(false);

	initAdd();

	//std::thread thread_obj(checkCondition);

	while (!GetAsyncKeyState(VK_DELETE))
	{
		if (GetAsyncKeyState(VK_F4)) { uav = false; Sleep(100); showLog(true); }
		if (GetAsyncKeyState(VK_F5)) { recoil = !recoil; Sleep(100); showLog(true); }
		if (GetAsyncKeyState(VK_F6)) { initAdd(); Sleep(100); showLog(true); }
		if (GetAsyncKeyState(VK_F7)) { fov = 25; showLog(true); }
		if (GetAsyncKeyState(VK_F8)) { fov = 50; showLog(true); }
		if (GetAsyncKeyState(VK_F9)) { fov = 100; showLog(true); }
		if (GetAsyncKeyState(VK_F10)) { aimbot = !aimbot; Sleep(100); showLog(true); }
		if (GetAsyncKeyState(VK_F11)) { aim_location = 5; showLog(true); }
		if (GetAsyncKeyState(VK_F12)) { aim_location = 7; showLog(true); }

		if (flag == false) continue;

		const auto bone_base_pos = decryption::get_bone_base_pos(sdk::client_info);

		if (bone_base_pos.x == 0 and bone_base_pos.y == 0 and bone_base_pos.z == 0) {
			flag = false;
			ingameFlag = sdk::in_game();
			showLog(false);
			std::cout << "Cannot found bone_base_pos... waiting for new game..." << std::endl;
			continue;
		}

		const auto bone_base = decryption::decrypt_bone_base(sdk::module_base, sdk::peb);

		renderer::scene::start();

		sdk::player_t local(sdk::client_info_base + (sdk::local_index() * offsets::player::size));
		auto local_pos = local.get_pos();
		//if (local_pos.x == 0 and local_pos.y == 0 and local_pos.z == 0) continue;

		auto local_team = local.team_id();

		vec2_t middle(resolution.x / 2 - 8, resolution.y / 2 - 14);

		renderer::scene::text(middle, L"+", renderer::colors::red_color, renderer::fonts::watermark_font);

		const auto name_base = local.get_name_array_base(sdk::module_base);
		if (!name_base or name_base == 0) continue;
		if (ingameFlag && uav)
		{
			aUAV();
		}
		sdk::StartTick();
		for (int i = 0; i < player_count; i++)
		{
			sdk::player_t player(sdk::client_info_base + (i * offsets::player::size));
			if (!player.is_valid() || player.dead())
			{
				continue;
			}

			if (player.team_id() == local_team)
			{
				continue;
			}

			vec2_t screen, player_head;
			vec3_t pos = player.get_pos();
			if (sdk::w2s(pos, screen))
			{
				auto dist = sdk::units_to_m(local_pos.distance_to(pos));

				//auto stance = static_cast<sdk::CharacterStance>(player.get_stance());

				if (dist > 0 && dist < 301)
				{

					sdk::name name_st = player.get_name_struct(i, name_base);
					if (name_st.health < 1) continue;
					std::string FullString;
					FullString = FullString + "[" + std::to_string(static_cast<int>(dist)) + "]";
					FullString = FullString + " " + name_st.name + "";

					bool isInside = isInSideFOV(screen, middle);

					bool isVisible = player.is_visible();

					DrawESP(screen, FullString, dist, isInside, isVisible);
					
					if (isInside or dist < 51)
					{
						if (recoil)
						{
							if (GetAsyncKeyState(VK_LBUTTON))
							{
								NoRecoil();
							}
						}
						const auto bone_index = decryption::get_bone_index(i, sdk::module_base);

						const auto bone_ptr = player.get_bone_ptr(bone_base, bone_index);

						if (!bone_ptr) {
							continue;
						}


						if (aimbot) {
							sdk::UpdateVelMap(i, pos);
							vec3_t bone_aim_location = decryption::get_bone_position(bone_ptr, bone_base_pos, aim_location);

							vec3_t predict = sdk::PredictionSolver(local_pos, bone_aim_location, i);

							vec2_t pos_aimbot, bone_aim;
							if (sdk::w2s(predict, pos_aimbot) && sdk::w2s(bone_aim_location, bone_aim)) {

								if (((bone_aim.x < (middle.x + fov)) and (bone_aim.x > (middle.x - fov))
									and (bone_aim.y < (middle.y + fov)) and (bone_aim.y > (middle.y - fov)) || i == actorAim) && isVisible)
								{
									actorAim = i;
									renderer::scene::ellipse(pos_aimbot.x, pos_aimbot.y, 4, renderer::colors::test_color);
									if (GetAsyncKeyState(VK_RBUTTON) && pos_aimbot.x > 0 && pos_aimbot.y > 0) {
										sdk::mousemove(pos_aimbot.x, pos_aimbot.y, resolution.x, resolution.y, 3);
									}

								}
							}
						}

						DrawBonesAndBox(bone_ptr, bone_base_pos, pos, isVisible, name_st.health, isInside);
						
					}
				}

			}
		}

		renderer::scene::end();

		sdk::ref_def = driver::read<sdk::ref_def_t>(decryption::get_ref_def());
	}
	renderer::shutdown();
	//thread_obj.detach();
	return 0;
}


