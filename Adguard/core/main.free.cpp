#include <iostream>
#include "driver/driver.h"
#include "game/offsets.h"
#include "overlay/hijack/hijack.h"
#include "overlay/renderer/renderer.h"
#include <thread>
#include <string>
#include <atlbase.h>
#include <atlconv.h>
#include <cstdlib>
#include <ctime>

#define ERROR(msg) std::cout << "[-] " << msg << std::endl; std::cin.get(); exit(EXIT_FAILURE); 
#define ASSERT(cond, msg) if (!cond) { ERROR(msg) }
vec2_t resolution(1920, 1080);
bool aimbot = false;
bool b1 = true;
int junk = 0;
bool flag = false;
bool ingameFlag = false;
int player_count = 1;
bool recoil = false;

#define J() UsefulFuntion(16, 45, 578, 4645, 12.3f);

inline int UsefulFuntion(int x, int y, int z, int r, float ks)
{
	srand(time(NULL));
	int res = rand();
	ks /= 2;
	for (int i = 2; i < 8; i++)
	{
		z += 456;
		r -= 55;
		x++;
		y--;

	}
	for (int i = 0; i < 2; i++)
	{
		x++;
		y += 234;
		z -= 23;
		r += 634;
	}
	junk = x + z + r + y + res;
	return junk;
}

void showLog(bool withAddress)
{
	system("CLS");
	std::cout << "By TekMonts#6635, working with version 1.45.2.10635118" << std::endl;
	std::cout << "Set game to window borderless, resolution is 1920x1080" << std::endl;
	std::cout << "[-] Hot Key" << std::endl;
	std::cout << "	[~] F5: No recoil enable: " << std::boolalpha << recoil << std::endl;
	std::cout << "	[~] F10: Aimbot enable: " << std::boolalpha << aimbot << std::endl;
	if (withAddress)
	{
		std::cout << "[-] Log" << std::endl;
		std::cout << "	[~] pid:  " << std::hex << sdk::process_id << std::endl;
		std::cout << "	[~] base: " << std::hex << sdk::module_base << std::endl;
		std::cout << "	[~] peb:  " << std::hex << sdk::peb << std::endl;
		std::cout << "	[~] hwnd: " << std::hex << sdk::hwnd << std::endl;
		std::cout << "	[~] client_info:      " << std::hex << sdk::client_info << std::endl;
		std::cout << "	[~] client_info_base: " << std::hex << sdk::client_info_base << std::endl;
		std::cout << "	[~] in game: " << std::boolalpha << sdk::in_game() << std::endl;
		std::cout << "	[~] player count: " << std::dec << sdk::player_count() << std::endl;
		std::cout << "	[~] junk: " << std::dec << junk << std::endl;
		std::cout << std::endl;
		if (!sdk::in_game())
		{
			std::cout << "Currently not in game... waiting for new game... " << std::endl;
		}
	}
	std::cout << std::endl;

}



int getPlayerCount()
{
	if (player_count > 4) return player_count;

	return 150;
}

void checkCondition()
{
	while (true) {
		Sleep(30000);
		if (sdk::in_game()) {
			ingameFlag = true;
			player_count = sdk::player_count();
			continue;
		}
		ingameFlag = false;
		flag = false;
	}
}

void initAdd()
{
	flag = false;
	J();
	sdk::client_info = decryption::get_client_info();
	if (!sdk::client_info or sdk::client_info == 0) {
		flag = false;
		showLog(false);
		std::cout << "Cannot found client_info... waiting for new game..." << std::endl;
		return;
	}
	const auto bone_base_pos = decryption::get_bone_base_pos(sdk::client_info);

	if (bone_base_pos.x == 0 and bone_base_pos.y == 0 and bone_base_pos.z == 0) {
		flag = false;
		showLog(false);
		std::cout << "Cannot found bone_base_pos... waiting for new game..." << std::endl;
		return;
	}
	sdk::client_info_base = decryption::get_client_info_base();
	auto ref_def_ptr = decryption::get_ref_def();
	if (!ref_def_ptr or ref_def_ptr == 0) {
		flag = false;
		showLog(false);
		std::cout << "Cannot found ref_def... waiting for new game..." << std::endl;
		return;
	}
	sdk::visible_base = sdk::get_visible_base(sdk::module_base);
	sdk::ref_def = driver::read<sdk::ref_def_t>(ref_def_ptr);
	showLog(true);
	b1 = true;
	flag = true;
};


int main() {
	sdk::process_id = driver::get_process_id("ModernWarfare.exe");
	ASSERT(sdk::process_id, "failed to find pid");

	sdk::module_base = driver::get_module_base_address("ModernWarfare.exe");
	ASSERT(sdk::module_base, "failed to module base");

	sdk::peb = driver::get_peb();
	ASSERT(sdk::peb, "failed to find peb");

	sdk::set_game_hwnd();
	ASSERT(sdk::hwnd, "failed to find window handle");

	ASSERT(hijack::init(), "failed to hijack nvidia overlay");
	ASSERT(renderer::init(), "failed to initlize renderer");
	std::string title = "WZCC Free version";
	SetConsoleTitle(title.c_str());
	showLog(false);

	initAdd();

	std::thread thread_obj(checkCondition);

	while (!GetAsyncKeyState(VK_DELETE))
	{

		if (GetAsyncKeyState(VK_F5)) { showLog(true); 
		std::cout << "This is free version, no recoil will not work..." << std::endl;
		}
		if (GetAsyncKeyState(VK_F10)) { showLog(true); 
		std::cout << "This is free version, aimbot will not work..." << std::endl;
		}

		if (ingameFlag) {
			initAdd();
			ingameFlag = !ingameFlag;
		}

		if (flag == false) continue;

		if (!sdk::client_info or sdk::client_info == 0) {
			flag = false;
			showLog(false);
			std::cout << "Cannot found client_info... waiting for new game..." << std::endl;
			continue;
		}

		const auto bone_base_pos = decryption::get_bone_base_pos(sdk::client_info);

		if (bone_base_pos.x == 0 and bone_base_pos.y == 0 and bone_base_pos.z == 0) {
			flag = false;
			showLog(false);
			std::cout << "Cannot found bone_base_pos... waiting for new game..." << std::endl;
			continue;
		}

		const auto bone_base = decryption::decrypt_bone_base(sdk::module_base, sdk::peb);

		renderer::scene::start();
		sdk::player_t local(sdk::client_info_base + (sdk::local_index() * offsets::player::size));
		auto local_pos = local.get_pos();
		if (local_pos.x == 0 and local_pos.y == 0 and local_pos.z == 0) continue;

		auto local_team = local.team_id();

		vec2_t middle(resolution.x / 2 - 8, resolution.y / 2 - 14);

		renderer::scene::text(middle, L"+", renderer::colors::red_color, renderer::fonts::watermark_font);

		const auto name_base = local.get_name_array_base(sdk::module_base);

		if (!name_base or name_base == 0) continue;

		for (int i = 0; i < getPlayerCount(); i++)
		{
			sdk::player_t player(sdk::client_info_base + (i * offsets::player::size));
			if (!player.is_valid() || player.dead())
			{
				continue;
			}

			if (player.team_id() == local_team)
			{
				continue;
			}
			
			vec2_t screen, player_head;
			vec3_t pos = player.get_pos();
			if (sdk::w2s(pos, screen))
			{
				auto dist = sdk::units_to_m(local_pos.distance_to(pos));
				if (dist > 1 && dist < 301)
				{
					sdk::name name_st = player.get_name_struct(i, name_base);
					std::string FullString;
					FullString = FullString + "[" + std::to_string(static_cast<int>(dist)) + "]";
					FullString = FullString + " " + name_st.name + "";
					CA2W pszWide(FullString.c_str(), CP_UTF8);
					bool isVisible = player.is_visible();

					bool isInside = isInSideFOV(screen, middle);

					DrawESP(screen, FullString, dist, isInside, isVisible);
					if (isInside)
					{
						const auto bone_index = decryption::get_bone_index(i, sdk::module_base); // player_index ranges from 0 to 150

						const auto bone_ptr = player.get_bone_ptr(bone_base, bone_index);


						if (!bone_ptr) {
							continue;
						}

						const auto bone_head = decryption::get_bone_position(bone_ptr, bone_base_pos, 7);
						const auto bone_2 = decryption::get_bone_position(bone_ptr, bone_base_pos, 2);

						const auto bone_6 = decryption::get_bone_position(bone_ptr, bone_base_pos, 6);

						const auto bone_6_10 = decryption::get_bone_position(bone_ptr, bone_base_pos, 10);
						const auto bone_10_11 = decryption::get_bone_position(bone_ptr, bone_base_pos, 11); //left hand
						const auto bone_11_12 = decryption::get_bone_position(bone_ptr, bone_base_pos, 12);

						const auto bone_6_14 = decryption::get_bone_position(bone_ptr, bone_base_pos, 14);
						const auto bone_14_15 = decryption::get_bone_position(bone_ptr, bone_base_pos, 15); //right hand
						const auto bone_15_16 = decryption::get_bone_position(bone_ptr, bone_base_pos, 16);

						const auto bone_2_18 = decryption::get_bone_position(bone_ptr, bone_base_pos, 18);
						const auto bone_18_19 = decryption::get_bone_position(bone_ptr, bone_base_pos, 19); //left foot


						const auto bone_2_22 = decryption::get_bone_position(bone_ptr, bone_base_pos, 22);
						const auto bone_22_23 = decryption::get_bone_position(bone_ptr, bone_base_pos, 23); //right foot


						DrawBone(bone_6, bone_head, pos, isVisible);
						DrawBone(bone_2, bone_6, pos, isVisible);
						DrawBone(bone_2, bone_2_18, pos, isVisible);
						DrawBone(bone_2_18, bone_18_19, pos, isVisible);
						DrawBone(bone_2, bone_2_22, pos, isVisible);
						DrawBone(bone_2_22, bone_22_23, pos, isVisible);
						DrawBone(bone_6, bone_6_10, pos, isVisible);
						DrawBone(bone_6_10, bone_10_11, pos, isVisible);
						DrawBone(bone_10_11, bone_11_12, pos, isVisible);
						DrawBone(bone_6, bone_6_14, pos, isVisible);
						DrawBone(bone_6_14, bone_14_15, pos, isVisible);
						DrawBone(bone_14_15, bone_15_16, pos, isVisible);
						DrawBox(bone_head, pos, bone_6_10, bone_6_14, bone_18_19, bone_22_23, name_st.health, isVisible);
					}
				}
			}
		}

		renderer::scene::end();

		sdk::ref_def = driver::read<sdk::ref_def_t>(decryption::get_ref_def());
	}
	renderer::shutdown();
	thread_obj.detach();
	return 0;
}


