
namespace offsets {
    constexpr auto adrRender = 0x133DF8E8; //48 8B 1D ? ? ? ? FF 50 58adrRender 0x133DF8E8
    //AdrApplication 0x132CC550
    constexpr auto Render_Matrix = 0xBF0;
    constexpr auto Render_Width = 0xF348; //+ 4 |Height
    constexpr auto Render_SwapChain = 0x137A8;
    constexpr auto AdrApplication = 0x132CC550; //48 8B 05 ? ? ? ? 48 85 C0 74 20 80 78 11 00 74 1A 48 8D 70 28 FF 56 10 48 85 C0 74 0E 8B 56 08 48 8B C8 E8 ? ? ? ? 48 8B F0 45 84 F6 0F 84 ? ? ? ? 48 85 F6 0F 84 ? ? ? ? 8B 0D
    constexpr auto ptrArray1 = 0x40;
    constexpr auto ArrayMap = 0x100;
    constexpr auto Object_Components = 0x10;
    constexpr auto Object_Transform = 0x30;
    constexpr auto Object_Name = 0x38;
    constexpr auto Object_CountName = Object_Name + 0x10;
    constexpr auto Transform_Matrix = 0xE8;
}