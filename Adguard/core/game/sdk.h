#pragma once
#include <cstdint>
#include <windows.h>
#include "..\utils\vectors.h"
#include <atlbase.h>
#include <atlconv.h>
#include <string>
#include <map>
#include "offsets.h"
#include "../overlay/renderer/renderer.h"

#define is_valid_ptr(p) ((uintptr_t)(p) <= 0x7FFFFFFEFFFF && (uintptr_t)(p) >= 0x1000) 
#define is_bad_ptr(p)	(!is_valid_ptr(p))

namespace sdk {
	extern DWORD64	 process_id;
	extern HWND      hwnd;
	extern DWORD nTickTime;//64 ms
	extern bool bUpdateTick;
	class Result
	{
	public:
		bool	hasResult;
		float	a;
		float	b;
	};
	struct velocityInfo_t
	{
		vec3_t		lastPos;
		vec3_t		delta;
	};

	extern std::map<int, velocityInfo_t> velocityMap;

	struct Vector
	{
		DWORD_PTR Begin;
		DWORD_PTR End;
	};
	
	struct AzList
    {
    	DWORD_PTR Begin;
    	DWORD_PTR Back;
    	DWORD_PTR Id;
    	DWORD_PTR Data;
    };

    struct Matrix34
    {
    	float m00, m01, m02, m03;
    	float m10, m11, m12, m13;
    	float m20, m21, m22, m23;

    vec3_t GetTranslation() { vec3_t mf; mf.x = m03; mf.y = m13; mf.z = m23; return vec3_t(mf); }

    };

    struct Matrix44
    {
    float _11, _12, _13, _14;
    float _21, _22, _23, _24;
    float _31, _32, _33, _34;
    float _41, _42, _43, _44;
    };

	void mousemove(float tarx, float tary, float X, float Y, int smooth);
	
	void set_game_hwnd();
	
	Result MidnightSolver(float a, float b, float c);

	bool WorldToScreen(vec3_t v, vec2_t &o, Matrix44 m);

	float units_to_m(float units);
	uint64_t GetNameList();
	void StartTick();

	void UpdateVelMap(int index, vec3_t vPos);

	void ClearMap();

	vec3_t GetSpeed(int index);

	vec3_t PredictionSolver(vec3_t local_pos, vec3_t position, int index);
}

void DrawBar(float x, float y, int value);
void FillRGB(float x, float y, float w, float h, int r, int g, int b, int a);
float Distance3D(vec3_t point1, vec3_t point2);
void DrawHeadCircle(vec3_t from, vec3_t to);
void DrawESP(vec2_t root, std::string fullName, float dist, bool isInSide);
bool isInSideFOV(vec2_t root, vec2_t middle);
void DrawBox(vec3_t head, vec3_t foot, vec3_t left_hand, vec3_t right_hand, vec3_t left_foot, vec3_t right_foot, int health, bool visible);
vec3_t posAimbot(vec3_t pos, int stance, bool aimHead, uintptr_t bone_ptr, vec3_t bone_base_pos, bool boneOK);