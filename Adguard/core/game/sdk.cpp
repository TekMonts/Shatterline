#include "sdk.h"

typedef unsigned char uint8;      /* Unsigned 8 bit quantity  */
typedef unsigned short uint16;    /* Unsigned 16 bit quantity */
typedef unsigned int uint32;
typedef unsigned long long  uint64;
typedef unsigned int uint;

#define BYTEn(x, n)   (*((_BYTE*)&(x)+n))
#define WORDn(x, n)   (*((_WORD*)&(x)+n))
#define WORD1(x)   WORDn(x,  1)
#define BYTE1(x)   BYTEn(x,  1)   
#define BYTE2(x) BYTEn(x, 2)

#define _BYTE uint8
#define _WORD uint16

namespace sdk {
	DWORD64	  process_id = NULL;
	HWND      hwnd = NULL;

	DWORD nTickTime = 64;//64 ms
	bool bUpdateTick = false;
	std::map<int, velocityInfo_t> velocityMap;
	BOOL CALLBACK enum_windows(HWND hwnd, LPARAM param) {
		DWORD process_id;
		GetWindowThreadProcessId(hwnd, &process_id);
		if (process_id == param)
		{
			sdk::hwnd = hwnd;
			return false;
		}
		return true;
	}

	void mousemove(float tarx, float tary, float X, float Y, int smooth)
	{
		float ScreenCenterX = (X / 2);
		float ScreenCenterY = (Y / 2);
		float TargetX = 0;
		float TargetY = 0;

		if (tarx != 0)
		{
			if (tarx > ScreenCenterX)
			{
				TargetX = -(ScreenCenterX - tarx);
				TargetX /= smooth;
				if (TargetX + ScreenCenterX > ScreenCenterX * 2) TargetX = 0;
			}

			if (tarx < ScreenCenterX)
			{
				TargetX = tarx - ScreenCenterX;
				TargetX /= smooth;
				if (TargetX + ScreenCenterX < 0) TargetX = 0;
			}
		}

		if (tary != 0)
		{
			if (tary > ScreenCenterY)
			{
				TargetY = -(ScreenCenterY - tary);
				TargetY /= smooth;
				if (TargetY + ScreenCenterY > ScreenCenterY * 2) TargetY = 0;
			}

			if (tary < ScreenCenterY)
			{
				TargetY = tary - ScreenCenterY;
				TargetY /= smooth;
				if (TargetY + ScreenCenterY < 0) TargetY = 0;
			}
		}
		mouse_event(MOUSEEVENTF_MOVE, static_cast<DWORD>(TargetX), static_cast<DWORD>(TargetY), NULL, NULL);
	}

	void set_game_hwnd() {
		EnumWindows(enum_windows, (LPARAM)sdk::process_id);
	}

	Result MidnightSolver(float a, float b, float c)
	{
		sdk::Result res;
		double subsquare = b * b - 4 * a * c;
		if (subsquare < 0)
		{
			res.hasResult = false;
		}
		else
		{
			res.hasResult = true,
				res.a = (float)((-b + sqrt(subsquare)) / (2 * a));
			res.b = (float)((-b - sqrt(subsquare)) / (2 * a));
		}

		return res;
	}

	bool WorldToScreen(vec3_t v, vec2_t &o, Matrix44 m)
    {
		vec2_t screenSize(1920, 1080);
        float w = v.x * m._14 + v.y * m._24 + v.z * m._34 + m._44;
        if (w < 0.0f)
            return false;

        float x = v.x * m._11 + v.y * m._21 + v.z * m._31 + m._41;
        float y = v.x * m._12 + v.y * m._22 + v.z * m._32 + m._42;

        x /= w;
        y /= w;

        o.x = (1.0f + x) * (screenSize.x / 2);
        o.y = (1.0f - y) * (screenSize.y / 2);

        return ((o.x >= 0.0f) && (o.x <= screenSize.x) && (o.y >= 0.0f) && (o.y <= screenSize.y));
    }

	float units_to_m(float units) {
		return units * 0.0254;
	}
	void StartTick()
	{
		static DWORD lastTick = 0;
		DWORD t = GetTickCount64();
		bUpdateTick = lastTick < t;
		if (bUpdateTick)
			lastTick = t + nTickTime;
	}

	void UpdateVelMap(int index, vec3_t vPos)
	{
		if (sdk::bUpdateTick)
		{
			velocityMap[index].delta = vPos - velocityMap[index].lastPos;
			velocityMap[index].lastPos = vPos;
		}
	}

	void ClearMap()
	{
		if (!velocityMap.empty())
			velocityMap.clear();
	}

	vec3_t GetSpeed(int index)
	{
		return velocityMap[index].delta;
	}


	vec3_t PredictionSolver(vec3_t local_pos, vec3_t position, int index)
	{
		vec3_t aimPosition = vec3_t(0, 0, 0);
		auto target_speed = GetSpeed(index);
		float bullet_speed = 567.0f;
		local_pos -= position;

		float a = (target_speed.x * target_speed.x) + (target_speed.y * target_speed.y) + (target_speed.z * target_speed.z) - ((bullet_speed * bullet_speed) * 100);
		float b = (-2 * local_pos.x * target_speed.x) + (-2 * local_pos.y * target_speed.y) + (-2 * local_pos.z * target_speed.z);
		float c = (local_pos.x * local_pos.x) + (local_pos.y * local_pos.y) + (local_pos.z * local_pos.z);

		local_pos += position;

		sdk::Result r = sdk::MidnightSolver(a, b, c);
		if (r.a >= 0 && !(r.b >= 0 && r.b < r.a))
		{
			aimPosition = position + target_speed * r.a;
		}
		else if (r.b >= 0)
		{
			aimPosition = position + target_speed * r.b;
		}

		return aimPosition;
	}
}

void DrawBar(float x, float y, int value)
{
	float l, r, g;
	g = value * 2.55;
	r = 255 - g;
	l = value / 3;
	FillRGB(x - (l / 2) - 1, y - 1, l + 13 + 2, 5, 0, 0, 0, 255);
	FillRGB(x - (l / 2), y, l + 13, 3, r, g, 0, 255);

}

void FillRGB(float x, float y, float w, float h, int r, int g, int b, int a)
{
	w /= 1.6f;
	vec2_t vLine[2];
	vLine[0].x = x + w / 2;
	vLine[0].y = y;
	vLine[1].x = x + w / 2;
	vLine[1].y = y + h;
	ID2D1SolidColorBrush* outColor;
	renderer::colors::getColor(D2D1::ColorF(r, g, b, a), outColor);
	renderer::scene::line(vLine[0].x, vLine[0].y, vLine[1].x, vLine[1].y, w, outColor);

}

float Distance3D(vec3_t point1, vec3_t point2)
{
	float distance = sqrt((point1.x - point2.x) * (point1.x - point2.x) +
		(point1.y - point2.y) * (point1.y - point2.y) +
		(point1.z - point2.z) * (point1.z - point2.z));
	return distance;
}

void DrawHeadCircle(vec3_t from, vec3_t to) {
	/*float size = Distance3D(from, to);
	if (size > 20)
		return;
	vec2_t W2S_from;
	if (!sdk::WorldToScreen(from, W2S_from))
		return;
	renderer::scene::ellipse(W2S_from.x, W2S_from.y - size, size * 1.6, renderer::colors::test_color);*/
}

void DrawESP(vec2_t root, std::string fullName, float dist, bool isInSide)
{
	vec2_t resolution(1920, 1080);
	CA2W pszWide(fullName.c_str(), CP_UTF8);
	ID2D1SolidColorBrush* color;
	ID2D1SolidColorBrush* color_df;
	if (isInSide)
	{
		color = renderer::colors::green_color;
		color_df = renderer::colors::green_color;
	}
	else
	{
		color = renderer::colors::red_color;
		color_df = renderer::colors::white_color;
	}
		renderer::scene::text(vec2_t(root.x - fullName.size() * 3, root.y), pszWide, color, renderer::fonts::tahoma_font);
		renderer::scene::line(resolution.x / 2, resolution.y - 5, root.x, root.y + 25, 1.0f, color);
}

bool isInSideFOV(vec2_t root, vec2_t middle)
{
	return ((root.x < (middle.x + 150)) and (root.x > (middle.x - 150)) and (root.y < (middle.y + 500)) and (root.y > (middle.y - 500)));
}

void DrawBox(vec3_t head, vec3_t foot, vec3_t left_hand, vec3_t right_hand, vec3_t left_foot, vec3_t right_foot, int health, bool visible)
{
	/*vec2_t p_head, p_foot, p_lh, p_rh, p_lf, p_rf;
	if (sdk::WorldToScreen(head, p_head) && sdk::WorldToScreen(foot, p_foot) && sdk::WorldToScreen(left_hand, p_lh)
		&& sdk::WorldToScreen(right_hand, p_rh) && sdk::WorldToScreen(left_foot, p_lf) && sdk::WorldToScreen(right_foot, p_rf))
	{
		float minWidth = min(min(min(min(min(p_head.x, p_foot.x), p_lf.x), p_rf.x), p_lh.x), p_rh.x);
		float maxWidth = max(max(max(max(max(p_head.x, p_foot.x), p_lf.x), p_rf.x), p_lh.x), p_rh.x);
		float minHeight = min(min(min(min(min(p_head.y, p_foot.y), p_lf.y), p_rf.y), p_lh.y), p_rh.y);
		float maxHeight = max(max(max(max(max(p_head.y, p_foot.y), p_lf.y), p_rf.y), p_lh.y), p_rh.y);

		float fWidth = (maxWidth - minWidth) + 20;
		float fHeight = (maxHeight - minHeight) + 18;
		ID2D1SolidColorBrush* color;
		if (visible)
		{
			color = renderer::colors::green_color;
		}
		else
		{
			color = renderer::colors::red_color;
		}

		if (fWidth < 110 and fHeight < 220) {
			if (health)
				DrawBar(minWidth + 10, minHeight - 15, health);
			renderer::scene::box(minWidth - 10, minHeight - 15, fWidth, fHeight, 1, color);
		}

	}*/

}



//void DrawRadar(vec2_t diff, vec2_t enemy, bool isVisible)
//{
//
//	float center = 209.0f;
//	float scale = 0.035714f;
//	float size = 360;
//
//	float yaw = -atan2(sdk::ref_def.view.axis[1].x, sdk::ref_def.view.axis[0].x) - M_PI / 2;
//	float x = center + (diff.length() * scale) * cos(yaw - atan2(diff.y, diff.x));
//	float y = center + (diff.length() * scale) * sin(yaw - atan2(diff.y, diff.x));
//
//	ID2D1SolidColorBrush* color;
//	if (isVisible)
//	{
//		color = renderer::colors::green_color;
//	}
//	else
//	{
//		color = renderer::colors::red_color;
//	}
//
//	if (x > center - size / 2 && x < center + size / 2 &&
//		y > center - size / 2 && y < center + size / 2) {
//
//		// Print triangle that points where the enemy is aiming (relative to local)
//		auto enemy_pointing = RAD2DEG(enemy.y) - yaw;
//		renderer::scene::line(enemy.x + 10 * cos(enemy_pointing), enemy.y + 10 * sin(enemy_pointing),
//			enemy.x + 10 * cos(enemy_pointing - DEG2RAD(150)), enemy.y + 10 * sin(enemy_pointing - DEG2RAD(150)), 1.0f, color);
//
//		renderer::scene::line(enemy.x + 10 * cos(enemy_pointing), enemy.y + 10 * sin(enemy_pointing),
//			enemy.x + 10 * cos(enemy_pointing + DEG2RAD(150)), enemy.y + 10 * sin(enemy_pointing + DEG2RAD(150)), 1.0f, color);
//
//		renderer::scene::line(enemy.x + 10 * cos(enemy_pointing - DEG2RAD(150)), enemy.y + 10 * sin(enemy_pointing - DEG2RAD(150)),
//			enemy.x + 10 * cos(enemy_pointing + DEG2RAD(150)), enemy.y + 10 * sin(enemy_pointing + DEG2RAD(150)), 1.0f, color);
//
//	}
//
//}
//}